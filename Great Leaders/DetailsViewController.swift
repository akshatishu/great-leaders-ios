//
//  DetailsViewController.swift
//  Great Leaders
//
//  Created by Akshat Goel on 02/10/15.
//  Copyright © 2015 StarkSky. All rights reserved.
//

import UIKit
import CoreData

class DetailsViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var leaderPhoto: UIImageView!
    @IBOutlet weak var nationality: UILabel!
    @IBOutlet weak var birthdate: UILabel!
    @IBOutlet weak var longDetail: UITextView!
    
    

    
    var databasePath = NSString()
    var LeaderList = leadersListView()
    var CategorySel = MenuView()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        //scrollView.scrollsToTop = false
        longDetail.scrollsToTop = false
      
        
        
      //  scrollView.clipsToBounds = false
      //  scrollView.autoresizesSubviews = true
        let filemgr = FileManager.default
        
        
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first! as NSString
        var databasePath = documentsPath.appendingPathComponent("db.sqlite")
        
        if !filemgr.fileExists(atPath: databasePath as String) {
            
            let contactDB = FMDatabase(path: databasePath as String)
            
            if contactDB == nil {
                print("Error: \(contactDB?.lastErrorMessage())")
            }
            

        }
        
    //    contentView.frame = CGRectMake(0, 0, self.view.frame.size.width, 1000);
        
        print(LeaderList.givePosition())
        print(CategorySel.passLeaderCat())
        
        var contactDB = FMDatabase(path: databasePath as String)

        
        databasePath = Bundle.main.path(forResource: "leaders", ofType: "sqlite")!
        
        contactDB = FMDatabase(path: databasePath as String)
        if (contactDB?.open())! {
            var querySQL:String = ""
            switch CategorySel.passLeaderCat(){
            case "BUSINESS":
                 querySQL = "SELECT * FROM BUSINESS_DB WHERE _id = \(LeaderList.givePosition()+1)"
                leaderPhoto.image = UIImage(named: "buss" + "\(LeaderList.givePosition()+1)" + ".jpg")
                
            case "POLITICS":
                 querySQL = "SELECT * FROM POLITICS_DB WHERE _id = \(LeaderList.givePosition()+1)"
               leaderPhoto.image = UIImage(named: "pol" + "\(LeaderList.givePosition()+1)" + ".jpg")
            case "RELIGION":
                 querySQL = "SELECT * FROM RELIGION_DB WHERE _id = \(LeaderList.givePosition()+1)"
                leaderPhoto.image = UIImage(named: "rel" + "\(LeaderList.givePosition()+1)" + ".jpg")
            case "RULERS":
                 querySQL = "SELECT * FROM RULERS_DB WHERE _id = \(LeaderList.givePosition()+1)"
                leaderPhoto.image = UIImage(named: "rul" + "\(LeaderList.givePosition()+1)" + ".jpg")
            case "SPORTS":
                querySQL = "SELECT * FROM SPORTS_DB WHERE _id = \(LeaderList.givePosition()+1)"
                leaderPhoto.image = UIImage(named: "sports" + "\(LeaderList.givePosition()+1)" + ".jpg")
            default:
                print("nothing")
            }
            
           
           
            
            let results:FMResultSet? = contactDB?.executeQuery(querySQL,
                withArgumentsIn: nil)
            
            if results?.next() == true {
                name.text = results?.string(forColumn: "NAME")
                nationality.text = "NATIONALITY - \(results!.string(forColumn: "NATIONALITY"))"
                birthdate.text = "BIRTH DATE - \(results!.string(forColumn: "DOB"))"
                // leaderPhoto.image = UIImage(named: "rel"+"1"+".jpg")
                
                
                
                
                
              //   longDetail.numberOfLines = 0
//                longDetail.lineBreakMode = .ByWordWrapping
                longDetail.text = results?.string(forColumn: "TEXT")
                
                let fixedWidth = longDetail.frame.size.width
                longDetail.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                let newSize = longDetail.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                var newFrame = longDetail.frame
                newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
                print(newFrame)
                
                
                longDetail.frame = newFrame;
               // longDetail.clipsToBounds = false
             //   longDetail.superview?.frame.size.height = longDetail.frame.size.height
               //               var newwFrame = longDetail.superview?.frame
//                newwFrame?.size = CGSize(width: fixedWidth, height: newSize.height)
//                longDetail.superview?.frame = newwFrame!
               
                
                
//                longDetail.text = "Lincoln was the 16th president of the United States and one of the great American leaders. His presidency was dominated by the American Civil War."
                
                print(scrollView)
                

            } else {
            }
            contactDB?.close()
        } else {
            print("Error: \(contactDB?.lastErrorMessage())")
        }

        scrollView.contentSize.height = name.frame.size.height + longDetail.frame.size.height + birthdate.frame.size.height + leaderPhoto.frame.size.height
        
        contentView.frame.size.height = name.frame.size.height + longDetail.frame.size.height + birthdate.frame.size.height + leaderPhoto.frame.size.height
        
        
        print(name.frame.size.height,longDetail.frame.size.height)
        print(scrollView.frame.size.height)
        print(contentView.frame.size.height)
        
    }
//        let appDel: AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
//       let context: NSManagedObjectContext! = appDel.managedObjectContext
//        
//        
//        let newUser = NSEntityDescription.insertNewObjectForEntityForName("Leader", inManagedObjectContext: context) as NSManagedObject
//        
//        newUser.setValue("akshat", forKey: "name")
//        newUser.setValue("Indian", forKey: "nationality")
//        
//      
//        
//        do{
//  try    context.save()
//        }catch{
//            print("yo error")
//        }
//        
//          print(newUser)
//        
//        let results:NSArray
//        
//        //to fetch data from database
//        let request = NSFetchRequest(entityName: "Leader")
//        request.returnsObjectsAsFaults = false
//        do{
//            results = try context.executeFetchRequest(request)
//        }catch{
//            print("hey error")
//        }
        
       
    
        
        
        

        // Do any additional setup after loading the view.
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
   
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
