//
//  leadersListView.swift
//  Great Leaders
//
//  Created by Akshat Goel on 30/09/15.
//  Copyright © 2015 StarkSky. All rights reserved.
//

import UIKit
 var position:Int = 0
class leadersListView: UITableViewController {
    
    var getLeader = MenuView()
   
    
    let business = ["ALFRED NOBEL", "ANDREW CARNEGIE", "BILL GATES", "HENRY FORD", "JOHN CADBURY", "JOHN D. ROCKFELLER","LARRY PAGE", "RICHARD BRANSON", "STEVE JOBS","WALTER E. DISNEY"]
    let politics  = ["ABRAHAM LINCOLN","AUNG SUU KYI","FIDEL CASTRO","JOHN F. KENNEDY","MARGARET THATCHER","MARTIN LUTHER KING Jr.","MOHANDAS GANDHI","NELSON MANDELA","VLADIMIR LENIN","WINSTON CHURCHILL"]
    let religion  = ["CONFUCIUS","DALAI LAMA","GAUTAM BUDDHA","JESUS OF NAZARETH","JOSEPH SMITH,Jr.","KRISHNA","MARTIN LUTHER","MOSES","MUHAMMAD OF MECCA","ZOROASTER"]
    let rulers = ["ADOLF HITLER","AKBAR","ALEXANDER THE GREAT","CATHERINE THE GREAT","FREDERICK THE GREAT","GENGHIS KHAN","JULIUS CAESAR","NAPOLEION BONAPARTE","PETER THE GREAT","QUEEN ELIZABETH"]
    let sports  = ["AYRTON SENNA","JACKIE JOYNER KERSIE","MARTINA NAVRATILOVA","MICHAEL JORDAN","MUHAMMAD ALI","PELE","ROGER FEDERER","SACHIN TENDULKAR","SIR STEVE REDGRAVE","TIGER WOODS"]
    var leaderarray = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let test = getLeader.passLeaderCat()
        self.title = test
        
       //.backgroundView = UIImageView(image: UIImage(named: "taylor-swift"))
        
       
        
        switch test{
        case "BUSINESS":
            leaderarray = business
            
            case "POLITICS":
            leaderarray  = politics
            
        case "RELIGION":
            leaderarray = religion
            
        case "RULERS":
            leaderarray  = rulers
            
        case "SPORTS":
            leaderarray = sports
            
            default:
            print("nothing")
            
        }
        
      
        
        
        
        


        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
//    func giveCategory()->String{
//        return test
//    }
    
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return leaderarray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "fruits", for: indexPath) 
            cell.textLabel?.text = leaderarray[(indexPath as NSIndexPath).row]

            
        
            return cell
        }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       position = (indexPath as NSIndexPath).row
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func givePosition()->Int{
        return position
    }

    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
